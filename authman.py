#!/usr/bin/env python3

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
Part of LAnPS.

Manages authentication.

USAGE: authman.py command credential-name

Command may be newpass or delpass. Newpass creates a new password with the given name, delpass removes the named password if it exists.
"""
from liblanps import auth
import sys
import tty
import termios

if sys.argv[1] == "newpass":
  old = termios.tcgetattr(sys.stdin)
  try:
    tty.setcbreak(sys.stdin)
    password = input("Enter the password: ")
    print()
    password2 = input("Enter password again: ")
    print()
  finally:
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old)
  if password != password2:
    print("Passwords must match. Try again.")
  auth.addPassword(sys.argv[2], password)
elif sys.argv[1] == "delpass":
  auth.removePassword(sys.argv[2])
