# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
Part of LAnPS.

Authentication handling module.
"""
from . import config, database
from icecream import ic
import os
from passlib.hash import argon2

def addPassword(name, password):
  hash = argon2.hash(password)
  curs = database.connection.cursor()
  curs.execute("INSERT INTO system.secrets VALUES (DEFAULT, 'password.hash', %s, %s);", (name, hash))
  curs.close()
  database.connection.commit()

def removePassword(name):
  curs = database.connection.cursor()
  curs.execute("DELETE FROM system.secrets WHERE name = %s AND type = 'password.hash';", (name,))
  curs.close()
  database.connection.commit()

def checkPassword(name, password):
  curs = database.connection.cursor()
  curs.execute("SELECT value FROM system.secrets WHERE name = %s AND type = 'password.hash';", (name,))
  hash = bytes(curs.fetchone()[0])
  curs.close()
  return argon2.verify(password, hash)
