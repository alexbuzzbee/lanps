# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
Part of LAnPS.

Configuration management module.
"""
import configparser
import pathlib
import sys

config = configparser.ConfigParser()
config.read(pathlib.Path(sys.path[0]) / "lanps.ini")

def get(key):
  parts = key.split(".", maxsplit=1)
  return config[parts[0]][parts[1]]
