# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
Part of LAnPS.

Database interface module.
"""
import psycopg2
from . import config

connection = psycopg2.connect(f"host='{config.get('database.dbSockDir')}' dbname='{config.get('database.dbname')}' user='{config.get('database.user')}'")

def getTables():
  tables = []
  curs = connection.cursor()
  curs.execute("SELECT schemaname, tablename FROM pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema' ORDER BY schemaname, tablename")
  for table in curs:
    tables.append(f"{table[0]}.{table[1]}")
  curs.close()
  return tables

def getTableSchema(table):
  nameParts = table.split(".")
  columns = []
  curs = connection.cursor()
  curs.execute("SELECT column_name FROM information_schema.columns WHERE table_schema = %s AND table_name = %s ORDER BY ordinal_position;", (nameParts[0], nameParts[1]))
  for col in curs:
    columns.append(col[0])
  curs.close()
  return columns
