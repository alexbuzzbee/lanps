# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
Part of LAnPS.

Email-sending capabilities.
"""
import pathlib
import sys
from . import config
from email.mime import multipart, text
import smtplib # FIXME: smtplib MUST be imported *and used* before psycopg2; otherwise, a segmentation fault occurs.

credFile = open(pathlib.Path(sys.path[0]) / config.get("mail.credentialsFile")) # Get credentials.
credentials = credFile.readlines()
credFile.close()

server = smtplib.SMTP(config.get("mail.smtpServer"), 587) # Connect to server.
server.starttls()
server.login(credentials[0], credentials[1]) # Login with credentials.

def sendMail(content, subject="LAnPS message", destination=config.get("mail.defaultRecipient")):
  message = multipart.MIMEMultipart() # Construct message.
  message["From"] = config.get("mail.sender")
  message["To"] = destination
  message["Subject"] = subject
  message.attach(text.MIMEText(content))

  server.send_message(message)
