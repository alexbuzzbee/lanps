#!/usr/bin/env python3

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
Part of LAnPS.

Read script. Run during batch window to load data into LAnPS database.

Opens all non-hidden files in a directory as CSVs, connects to LAnPS database, and copies from files to database.
Cleans up after itself by truncating files to just their headers when done.
"""
import csv
import os
import sys
import pathlib
import datetime
from liblanps import config, database

"""
Works out a table schema and create the table.
"""
def setupTable(name, fieldNames, curs):
  schema = "id serial PRIMARY KEY"

  for field in fieldNames:
    if field == "datetime": # Handle datetime fields.
      schema += ", datetime timestamp"
    else:
      schema += f", {field} text"

  curs.execute(f"CREATE TABLE IF NOT EXISTS raw.{name} ({schema});")

"""
Determines the table name given the path to a CSV.
"""
def getTable(path):
  return pathlib.Path(path).stem

"""
Processes a single CSV file and loads it into the database.
"""
def processFile(path):
  print(f"Read file {path}.")

  curs = database.connection.cursor() # Initialize.
  table = getTable(path)
  dialect = csv.excel()
  dialect.quotechar = "␟"
  originalData = csv.DictReader(open(path, "r", encoding="utf-8"), dialect=dialect)
  setupTable(table, originalData.fieldnames, curs)

  values = [] # Collect data.
  for record in originalData:
    values.append(list(record.values()))
  
  curs.executemany(f"INSERT INTO raw.{table} VALUES (DEFAULT{', %s' * len(originalData.fieldnames)});", values) # Insert data.
  database.connection.commit()
  curs.close()

  open(path, "w", encoding="utf-8").write(",".join(originalData.fieldnames)) # Write over original file to "reset" it.

"""
Processes a directory of CSVs.
"""
def processDirectory(path):
  for item in pathlib.Path(path).iterdir():
    if item.name.startswith(".") or item.is_dir(): # Ignore directories and hidden files.
      continue
    try:
      processFile(item)
    except Exception as e:
      print(f"Error while processing {item.name}: {e}. Continuing. Correct the file and try again manually.", file=sys.stderr)

if __name__ == "__main__":
  if len(sys.argv) > 1:
    processDirectory(sys.argv[1])
  else:
    processDirectory(config.get("ifttt.csvs"))
