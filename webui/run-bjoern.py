#!/usr/bin/env python3

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
Part of LAnPS.

Runs the Web UI using the bjoern WSGI server.
"""
from liblanps import config
import bjoern
import socket
import application

app = application.start()

if config.get("webui.ipVersion") == "4":
  af = socket.AF_INET
elif config.get("webui.ipVersion") == "6":
  af = socket.AF_INET6
else:
  raise Exception("Unknown IP version.")

sock = socket.socket(af, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
sock.bind((config.get("webui.host"), int(config.get("webui.port"))))

try:
  bjoern.server_run(sock, app)
except KeyboardInterrupt:
  print("Exiting.")
finally:
  sock.close()
