#!/bin/bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Generates a self-signed certificate valid for one year on DNS name localhost and IP address 127.0.0.1.
openssl req -x509 -newkey rsa:4096 -sha256 -days 365 -nodes -keyout tlskey.pem -out tlscert.pem -extensions san -config <(echo "[req]"; echo distinguished_name=req; echo "[san]"; echo subjectAltName=DNS:localhost,IP:127.0.0.1) -subj /CN=localhost
