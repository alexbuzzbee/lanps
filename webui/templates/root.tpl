<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->
%rebase("templates/main.tpl", title="Home")
<p>Tables:</p>
<ul>
  %for name in tables:
  <li><a href="/table/{{name}}">{{name}}</a></li>
  %end
</ul>