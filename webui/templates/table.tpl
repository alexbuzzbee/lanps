<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->
%rebase("templates/main", title=f"Table {table}")
<table>
  <tr>
    %for column in schema:
    <th>{{column}}</th>
    %end
  </tr>
  %for record in data:
  <tr>
    %for cell in record:
    <td>{{cell}}</td>
    %end
  </tr>
  %end
</table>