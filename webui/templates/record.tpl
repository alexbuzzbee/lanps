<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->
%rebase("templates/main.tpl", title=f"Record {id} of {table}")
<a href="/table/{{table}}/">Back</a>
{{include("templates/record_" + table + ".tpl", record=data)}}