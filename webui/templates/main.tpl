<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LAnPS - {{title}}</title>
  </head>
  <body>
    <h1>LAnPS web interface</h1>
    <h2>{{title}}</h2>
    {{!base}}
    <style>
    body {
      font-family: sans-serif;
    }
    table, th, td {
      border: 1px solid;
      border-collapse: collapse;
    }
    </style>
  </body>
</html>