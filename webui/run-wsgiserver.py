#!/usr/bin/env python3

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
Part of LAnPS.

Runs the Web UI using WSGIserver.
"""
from liblanps import config
import bottle
import wsgiserver
import application

app = application.start()

cert = config.get("webui.certFile")
key = config.get("webui.keyFile")
ipVersion = config.get("webui.ipVersion")

if ipVersion == "4":
  if cert != "":
    server = wsgiserver.WSGIServer(app, host=config.get("webui.host"), port=int(config.get("webui.port")), certfile=cert, keyfile=key)
  else:
    server = wsgiserver.WSGIServer(app, host=config.get("webui.host"), port=int(config.get("webui.port")))
elif ipVersion == "6":
  if cert != "":
    server = wsgiserver.WSGIServer(app, host=config.get("webui.host6"), port=int(config.get("webui.port6")), certfile=cert, keyfile=key)
  else:
    server = wsgiserver.WSGIServer(app, host=config.get("webui.host6"), port=int(config.get("webui.port6")))
else:
  raise Exception("Unknown IP version " + ipVersion)

server.start()