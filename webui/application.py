#!/usr/bin/env python3

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
Part of LAnPS.

The web UI application. Uses Bottle.
"""
import bottle
from bottle import route, auth_basic
from liblanps import config, database, auth

def _lanpsAuth():
  return auth_basic(auth.checkPassword, realm="lanps", text="Login incorrect")
lanpsAuth = _lanpsAuth()

@route("/table/<table>")
@lanpsAuth
def table(table):
  data = []
  curs = database.connection.cursor()
  curs.execute(f"SELECT * FROM {table};")
  data = curs.fetchall()
  return bottle.template("templates/table.tpl", schema=database.getTableSchema(table), data=data, table=table)

@route("/")
@lanpsAuth
def root():
  return bottle.template("templates/root.tpl", tables=database.getTables())

def start():
  return bottle.default_app()

if __name__ == "__main__":
  print("Warning: Running using Bottle development server. May be slow and TLS won't work.")
  bottle.run(host=config.get("webui.host"), port=int(config.get("webui.port")), debug=True)