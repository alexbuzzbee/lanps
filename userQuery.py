#!/usr/bin/env python3

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
Part of LAnPS.

Run SQL queries submitted by the user.
"""
import csv
import pathlib
import sys
import tabulate
from liblanps import config, mail, database

def formatResultSet(resultSet):
  return tabulate.tabulate(resultSet, tablefmt="fancy_grid")

def runQuery(query):
  curs = database.connection.cursor()
  curs.execute(query)
  fields = tuple([description[0] for description in curs.description])
  resultSet = curs.fetchall()
  resultSet.insert(0, fields)
  return resultSet

def runFile(path):
  report = ""

  dialect = csv.excel()
  dialect.quotechar = "␟"
  data = csv.DictReader(open(path, "r", encoding="utf-8"))

  for record in data:
    report += f"Results of query requested {record['datetime']}. Query was:\n\n{record['query']}\n\n"
    report += formatResultSet(runQuery(record["query"]))
    report += "\n\n"
  
  if report != "":
    mail.sendMail(report, "LAnPS query results")
  
  open(path, "w", encoding="utf-8").write(",".join(data.fieldnames)) # Write over original file to "reset" it.

if __name__ == "__main__":
  if len(sys.argv) > 1:
    runFile(sys.argv[1])
  else:
    runFile(pathlib.Path(config.get("ifttt.csvs")) / "special" / "queries.txt")
