#!/usr/bin/env python3

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
Part of LAnPS.

Sets up the database.
"""
import psycopg2
from liblanps import config

connection = psycopg2.connect(f"dbname='postgres' user='{config.get('database.user')}'") # Connect to Postgres' default database.

curs = connection.cursor() # Create LAnPS database.
curs.execute(f"CREATE DATABASE {config.get('database.dbname')};")
connection.commit()
curs.close()
connection.close()

from liblanps import database # Now that the LAnPS database exists, we can use the module.

curs = database.connection.cursor()

curs.execute("CREATE SCHEMA raw;") # Create schemas.
curs.execute("GRANT ALL ON SCHEMA raw TO PUBLIC;")
curs.execute("COMMENT ON SCHEMA raw IS 'LAnPS data in \"raw\" form'")

curs.execute("CREATE SCHEMA cooked;")
curs.execute("GRANT ALL ON SCHEMA cooked TO PUBLIC;")
curs.execute("COMMENT ON SCHEMA raw IS 'LAnPS data in processed form'")

curs.execute("CREATE SCHEMA system;")
curs.execute("GRANT ALL ON SCHEMA system TO PUBLIC;")
curs.execute("COMMENT ON SCHEMA system IS 'LAnPS system data'")

curs.execute("DROP SCHEMA public;")

curs.execute("""CREATE TABLE cooked.ideas (
  id serial,
  idea text,
  datetime timestamp without time zone,
  "origin_table" text,
  "origin_id" integer,
  PRIMARY KEY (id)
)
""")

curs.execute("""CREATE TABLE system.analysis_status (
  "table_name" text COLLATE pg_catalog."default" NOT NULL,
  last_id integer,
  CONSTRAINT analysis_status_pkey PRIMARY KEY ("table_name")
)
""")

curs.execute("""
  INSERT INTO system.analysis_status
    VALUES ('raw.notes', 0)
""")

curs.execute("""
  INSERT INTO system.analysis_status
    VALUES ('raw.tweets', 0)
""")

curs.execute("""CREATE TABLE system.scheduled (
  id serial,
  type text,
  "time" time without time zone,
  datetime timestamp without time zone,
  task text,
  PRIMARY KEY (id)
)
""")

curs.execute("""CREATE TABLE system.secrets (
  id serial,
  type text,
  name text,
  value bytea,
  PRIMARY KEY (id)
)
""")

database.connection.commit()
curs.close()
