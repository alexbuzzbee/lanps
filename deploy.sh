# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Upload to LAnPS machine, then run to install or upgrade LAnPS.

if [ -d ~/lanps ]; then
  cd ~/lanps
  git pull
else
  git clone https://gitlab.com/alexbuzzbee/lanps.git lanps
  echo "Adding cron job. Use crontab -e to inspect and update if necessary."
  (crontab -l ; echo "0	*	*	*	*	$HOME/lanps/scheduled.py") | crontab -
  echo "As this is a new installation, you will want to update lanps.ini."
fi
