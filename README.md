# LAnPS

The Life Automation and Planning System, or LAnPS, is a database-driven personal automation and management system. It collects data from a number of sources, analyzes it to find useful pieces of information, and stores it in a database. A Web UI then allows you to view and work with the data.

LAnPS is licensed under the Mozilla Public License, a permissive copyleft license compatible with the GPL. The MPL is file-level, so you are free to use files from this project alongside proprietary, GPL, MIT License, or other code. Be aware that doing so does not free you from the requirements of any of those licenses. See [Mozilla's FAQ](https://www.mozilla.org/en-US/MPL/2.0/FAQ/) for more information.