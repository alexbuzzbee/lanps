#!/usr/bin/env python3

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
Part of LAnPS

Runs scheduled operations. Triggered by cron once an hour.
"""
import subprocess
import datetime
import time
import pathlib
import sys
from liblanps import config, database

def getTaskArgv(taskString):
  argv = taskString.split(" ")
  argv[0] = str(pathlib.Path(sys.path[0]) / (argv[0] + ".py"))
  return argv

curs = database.connection.cursor()
curs.execute("SELECT task FROM system.scheduled WHERE type = 'hourly';")

for task in curs:
  subprocess.Popen(getTaskArgv(task[0]))

curs.close()
curs = database.connection.cursor()
curs.execute("SELECT task, time FROM system.scheduled WHERE type = 'daily' AND time >= (current_time - INTERVAL '1' MINUTE) AND time < (current_time + INTERVAL '59.5' MINUTE);")

for task in curs:
  if task[1] > datetime.datetime.now().time():
    n = datetime.datetime.now().time() # Shorter names.
    t = task[1]
    length = (t.second + (t.minute * 60) + (t.hour * 3600)) - (n.second + (n.minute * 60) + (t.hour * 3600)) # Calculate time until task.
    cmds = f"sleep {length}; {' '.join(getTaskArgv(task[0]))}\n"
    subprocess.Popen(["sh", "-c", cmds], stdin=subprocess.PIPE)
  else:
    subprocess.Popen(getTaskArgv(task[0]))

curs.close()
curs = database.connection.cursor()
curs.execute("SELECT task, datetime FROM system.scheduled WHERE type = 'specific' AND datetime >= (current_timestamp - INTERVAL '1' MINUTE) AND datetime < (current_timestamp + INTERVAL '59.5' MINUTE);")

for task in curs:
  if task[1] > datetime.datetime.now():
    length = task[1].timestamp() - datetime.datetime.now().timestamp() # Calculate time until task.
    cmds = f"sleep {length}; {' '.join(getTaskArgv(task[0]))}\n"
    subprocess.Popen(["sh", "-c", cmds], stdin=subprocess.PIPE)
  else:
    subprocess.Popen(getTaskArgv(task[0]))
