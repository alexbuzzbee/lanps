#!/usr/bin/env python3

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
Part of LAnPS.

Analyze the data in the database.
"""
import re
import datetime
from liblanps import config, database

ideaRe = re.compile(config.get("analysis.ideas.regex"), flags=re.I)
ideaTables = config.get("analysis.ideas.tables").split(",")
ideaFields = {}
for table in ideaTables:
  ideaFields[table] = config.get(f"analysis.ideas.field.{table}")

"""
Analyzes a single record for ideas and return a list of them.
"""
def analyzeForIdeas(record, table):
  results = []
  data = record[int(ideaFields[table])]
  for idea in ideaRe.findall(data):
    results.append({
      "destTable": "cooked.ideas",
      "record": [idea, datetime.datetime.now(), table, record[0]]
    })
  return results

"""
Analyzes multiple records from a table and return a list of records to insert into cooked tables.
"""
def analyzeRecords(records, table):
  results = []
  if table in ideaTables:
    for record in records:
      results += analyzeForIdeas(record, table)
  return results

"""
Gets an open cursor from the database for analysis.
"""
def getRecords(table):
  curs = database.connection.cursor()
  curs.execute(f"SELECT * FROM {table} WHERE id > (SELECT last_id FROM system.analysis_status WHERE analysis_status.table_name = '{table}');")
  return curs.fetchall()

"""
Saves the records resulting from analysis into the database.
"""
def saveAnalysisResults(records):
  insertions = {}
  curs = database.connection.cursor()
  for record in records:
    if not record["destTable"] in insertions:
      insertions[record["destTable"]] = []
    insertions[record["destTable"]].append(record["record"])
  for key in insertions.keys():
    curs.executemany(f"INSERT INTO {key} VALUES (DEFAULT{', %s' * len(insertions[key][0])});", insertions[key])
  database.connection.commit()

"""
Updates analysis_status.last_id for the table specified based on the IDs in the given records.
"""
def updateAnalysisStatus(table, records):
  lastId = 0
  for record in records:
    if record[0] > lastId:
      lastId = record[0]
  curs = database.connection.cursor()
  curs.execute("UPDATE system.analysis_status SET last_id = %s WHERE analysis_status.table_name = %s;", (lastId, table))
  database.connection.commit()

"""
Run a complete analysis of a table.
"""
def analyzeTable(table):
  records = getRecords(table)
  found = analyzeRecords(records, table)
  saveAnalysisResults(found)
  updateAnalysisStatus(table, records)

"""
Run a complete analysis of all tables.
"""
def analyze():
  tables = ideaTables
  for table in tables:
    analyzeTable(table)

if __name__ == "__main__":
  analyze()
